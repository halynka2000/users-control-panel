<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	<title>Document</title>
</head>

<body>

<div class="container">
<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/configs/db.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/options/add-user.php';
?>
	<div class="row mt-3">
		<div class="col-lg-12">
			<div class="main-box clearfix">
				<div class="table-responsive" id="user">
					
						<?php
							include $_SERVER['DOCUMENT_ROOT'] . "/parts/table.php";
						?>
				</div>
			</div>
		</div>
	</div>

<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/options/add-user.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/parts/modal.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/parts/confirm.php';
?>
</div>



<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
<script src="/js/script.js"></script>
</body>

</html>