//имя пользователя
var $firstName = $("#first_name");
//фамилия пользователя
var $lastName = $("#last_name");
//статус: активный или нет
var $status = $("#switchOption");
//функция для редактирования пользователя
$("body").on("click", ".edit", function () {
	var id = $(this).val();
	$('#modal .user').val(id);
	$(".md-title").text('Edit user');
	$(".btn-save").text('Save');
	console.log($('.user').val());
	$.ajax({
		url: "../parts/edit-modal.php",
		type: "GET",
		data: {
			"id": $('.user').val()
		},
		success: function (response) {
			console.log(response);
			var jsonData = JSON.parse(response);
			$firstName.attr("value", jsonData.first_name);
			$lastName.attr("value", jsonData.last_name);
			if (jsonData.active == 1) {
				$status.prop('checked', true);
			} else {
				$status.prop('checked', false);
			}
			$("#role").val(jsonData.user_role);
			$(".form-modal").css("display", "block");
		},
		error: function () {
			alert("Error!");
		}
	});

	var $form = $("#modal");
	$form.one("submit", function (e) {
		e.preventDefault();
		if ($("#role option:selected").val() == 0 || $firstName.val() == 0 || $lastName.val() == 0) {
			$(".opt-title").text("Data Confirmation");
			$(".opt-body").text("Please, provide all the data");
			$(".option-confirm").css("display", "block");
		} else {
			$.ajax({
				url: "../options/edit-user.php",
				type: "POST",
				data: {
					"id": $('.user').val(),
					"firstName": $firstName.val(),
					"lastName": $lastName.val(),
					"status": $status.is(':checked') ? 1 : 0,
					"role": $("#role option:selected").val()
				},
				success: function () {
					$(".form-modal").css("display", "none");
					$("#user").load("../parts/table.php");
					$('#modal')[0].reset();
				},
				error: function () {
					alert("Error!");
				}
			});
		}
	});
});

//функция для добавления пользователя
$("body").on("click", ".add", function () {
	$(".md-title").text("Add user");
	$(".btn-save").text("Add");
	$("#modal .user").val(0);
	$firstName.removeAttr("value");
	$lastName.removeAttr("value");
	$status.prop('checked', false);
	$("#role").val("0");
	$(".form-modal").css("display", "block");
	var $modal = $("#modal");
	$modal.one("submit", function (ev) {
		ev.preventDefault();
		if ($("#role option:selected").val() == 0 || $firstName.val() == 0 || $lastName.val() == 0) {
			$(".opt-title").text("Data Confirmation");
			$(".opt-body").text("Please, provide all the data");
			$(".option-confirm").css("display", "block");
		} else {
			$.ajax({
				url: "../options/add-user.php",
				type: "POST",
				data: {
					"firstName": $firstName.val(),
					"lastName": $lastName.val(),
					"status": $status.is(':checked') ? 1 : 0,
					"role": $("#role option:selected").val()
				},

				success: function () {
					$(".form-modal").css("display", "none");
					$("#user").load("../parts/table.php");
					$('#modal')[0].reset();
				},
				error: function () {
					alert("Error!");
				}
			});
		}
	});
});

//фуннкция для удаления пользователя
$("body").on("click", ".delete", function () {
	var id = $(this).val();
	$('.confirm .delete-user').val(id);
	$(".confirm").css("display", "block");
	$(".yes").on("click", function (ev) {
		ev.preventDefault();
		$.ajax({
			url: "../options/delete-user.php",
			type: "GET",
			data: {
				"id": $('.delete-user').val()
			},
			success: function () {
				$(".confirm").css("display", "none");
				$("#user").load("../parts/table.php");
			},
			error: function () {
				alert("Error!");
			}
		});
	});
});

//закрытие модального окна
$(".exit").click(function () {
	$(".form-modal").css("display", "none");
	$(".confirm").css("display", "none");
});

$(".exit-confirm").click(function () {
	$(".option-confirm").css("display", "none");
});

//когда кликнули на чекбокс select all
$("body").on('click', 'thead :checkbox', function () {
	var check = this;
	$(this).parents('table').find('tbody :checkbox').each(function () {
		if ($(check).is(':checked')) {
			$(this).prop('checked', true);
		} else {
			$(this).prop('checked', false);
		}
	});
});

//для выбора чекбокса возле каждого пользователя
$("body").on('click', 'tbody :checkbox', function () {
	var parents = $(this).parents('table');
	if (!$(this).is(':checked')) {
		$(parents).find('thead :checkbox').prop('checked', false);
	}

	if (!$('input.check[type=checkbox]:not(:checked)').length) {
		$(parents).find('thead :checkbox').prop('checked', true);
	}
});

//когда подвердили выбранное действие из select
$(".btn-ok").on('click', function () {
	var select = $('.control').find('option:selected').val()
	if ($('input.check[type=checkbox]:checked').length == 0) {
		$(".opt-title").text("User Confirmation");
		$(".opt-body").text("Please, select users");
		$(".option-confirm").css("display", "block");
	} else {
		var arr = [];
		$('input.check[type=checkbox]:checked').each(function () {
			arr.push($(this).val());
		});
		if (select == 0) {
			$(".opt-title").text("Select Confirmation");
			$(".opt-body").text("Please, select option");
			$(".option-confirm").css("display", "block");
		}

		if (select == 3) {
			$(".confirm").css("display", "block");
			$(".yes").on("click", function (ev) {
				ev.preventDefault();
				$.ajax({
					url: "../options/select.php",
					type: "POST",
					data: {
						"users_id": arr,
						"select": select
					},
					success: function () {
						$(".confirm").css("display", "none");
						$("#user").load("../parts/table.php");
					},
					error: function () {
						alert("Error!");
					}
				});
			});
		}
		if (select == 1 || select == 2) {
			$.ajax({
				url: "../options/select.php",
				type: "POST",
				data: {
					"users_id": arr,
					"select": select
				},
				success: function () {
					$("#user").load("../parts/table.php");
				},
				error: function () {
					alert("Error!");
				}
			});
		}
	}
});

//смена значений в двух select
$(function () {
	var select = $('.control');
	select.change(function () {
		select.not(this).val(this.value);
	});
});