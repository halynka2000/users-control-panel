<table class="table user-list" >
	<thead>
		<tr>
			<th class="form-check">
				<span>
					<input type="checkbox" class="form-check-input" id="exampleCheck1">
					<label class="form-check-label" for="exampleCheck1">Select all</label>
				</span>
			</th>
			<th><span>First Name</span></th>
			<th><span>Last Name</span></th>
			<th class="text-center"><span>Status</span></th>
			<th><span>Role</span></th>
			<th><span>Options</span></th>
		</tr>
	</thead>
	<tbody>
		<?php
		include $_SERVER['DOCUMENT_ROOT'] . '/configs/db.php';

		//запрос для вывода всех пользователей
		$sql = "SELECT * FROM users";
		//исполнение запроса
		$result = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_assoc($result)){  
		?>	
			<form action="index.php" method="GET" id="fm">		
			<tr>
				<td>
					<input  value="<?php echo $row['id'];?>" type="checkbox" class="check form-check-input float-left ml-2">
				</td>
				<td name="first_name" value ="<?php echo $row['first_name']; ?>"class="text-center">
					<?php echo $row['first_name'] ?>
				</td>
				<td name="last_name" value ="<?php echo $row['last_name']; ?>" class="text-center">
					<?php echo $row['last_name'] ?>
				</td>
				<td class="status">
					<?php 
						if($row['active'] == 1){
					?>
						<span class="active"></span>
					<?php
						} else {
					?>
						<span></span>
					<?php
						}
					?>
				</td>
				<td name="role" value ="<?php echo $row['user_role']; ?>">
					<?php echo $row['user_role'] ?>
				</td>
					<td>
						<button type="button" class="btn btn-primary btn-sm edit" value="<?php echo $row['id']; ?>">
							<i class="fa fa-pencil" ></i>
						</button>
						<button type="button" class="btn btn-danger btn-sm delete" value="<?php echo $row['id']; ?>">
							<i class="fa fa-trash " aria-hidden="true"></i>
						</button> 
					</td>
				</tr>
			<?php
				}
			?>
		</form>
	</tbody>
</table>