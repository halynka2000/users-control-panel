<div class="modal confirm" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Confirmation</h5>
        <button type="button" class="close exit" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" value='' class="delete-user">
        <p>Are you sure you want to delete the user?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary yes">Yes</button>
        <button type="button" class="btn btn-secondary exit" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<div class="modal option-confirm" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title opt-title">User Confirmation</h5>
        <button type="button" class="close exit-confirm" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body opt-body">
        <p>Please, select user</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary exit-confirm" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>