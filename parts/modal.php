<div class="modal form-modal" tabindex="-1" role="dialog" id="myModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		
		<div class="modal-header">
			<h5 class="modal-title md-title">Enter user data </h5>
			<button type="button" class="close exit" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<form action="index.php" method="POST" id="modal">
			<div class="modal-body">
				<input type="hidden" value='' class="user">
				<div class="form-group">
					<label for="first_name" class="col-form-label">First Name:</label>
					<input type="text" class="form-control" id="first_name">
				</div>
				<div class="form-group">
					<label for="last_name" class="col-form-label">Last Name:</label>
					<input type="text" class="form-control" id="last_name"></input>
				</div>
				<div class="material-switch">
					<label>Status:</label>
					<input id="switchOption" name="switchOption" type="checkbox"/>
            		<label for="switchOption" class="label-primary"></label>
                </div>
				<div class="form-group">
				<label>Role:</label>
					<select class="form-control" id="role">
						<option value="0" selected="selected">Not selected</option>
						<option value="admin">admin</option>
						<option value="user">user</option>

					</select>
				</div>
			</div>
				
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-save">Save changes</button>
				<button type="button" class="btn btn-secondary exit">Close</button>
			</div>
		</form>
		</div>
	</div>
	</div>